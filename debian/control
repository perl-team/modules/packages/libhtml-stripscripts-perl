Source: libhtml-stripscripts-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Étienne Mollier <emollier@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libtest-pod-coverage-perl,
                     libtest-pod-perl,
                     perl
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libhtml-stripscripts-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libhtml-stripscripts-perl.git
Homepage: https://metacpan.org/release/HTML-StripScripts
Rules-Requires-Root: no

Package: libhtml-stripscripts-perl
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${perl:Depends}
Description: module for removing scripts from HTML
 HTML::StripScripts is a Perl module for neutralizes scripting constructs in
 HTML, leaving as much non-scripting markup in place as possible. This allows
 web applications to display HTML originating from an untrusted source without
 introducing cross-site scripting (XSS) vulnerabilities.
 .
 The process is based on whitelists of tags, attributes and attribute values.
 This approach is the most secure against disguised scripting constructs hidden
 in malicious HTML documents.
 .
 As well as removing scripting constructs, this module ensures that there is a
 matching end for each start tag, and that the tags are properly nested.
 .
 You will probably use HTML::StripScripts::Parser rather than using this module
 directly (see libhtml-stripscripts-parser-perl).
